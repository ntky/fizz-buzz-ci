const fizz_buzz = require('./fizz_buzz')

test('1..5', () => {
  expect(fizz_buzz([1, 2, 3, 4, 5])).toEqual(["1", "2", "fizz", "4", "buzz"])
})